# Stage 1
FROM node:10 as builder

COPY . /angularnew

WORKDIR /angularnew

RUN npm install
RUN $(npm bin)/ng build

# Stage 2 - the production environment
FROM nginx:alpine

COPY --from=builder /angularnew/dist/* /usr/share/nginx/html

EXPOSE 80